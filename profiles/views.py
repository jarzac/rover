#!/usr/bin/env python
# coding: utf-8
"""
Views for Rover exercise

Author: Juan Arzac
Date: June, 2016
"""

import numpy as np
from .models import Sitter
from django.shortcuts import render


def index(request):
    """
    View that gets data for the List of Sitters. It calls
    Args:
        request: request object

    Returns:
        render: render action that passes arguments to a template
    """
    sitters = Sitter.objects.all()
    rank_range = list(np.arange(0, 5, .5))

    return render(request, 'sitters_list.html', {'sitters': sitters,
                                                 'rank_range': rank_range})

