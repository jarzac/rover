#!/usr/bin/env python
# coding: utf-8
"""
Script to execute the data import from a csv file

Author: Juan Arzac
Date: June, 2016
"""

import csv
import datetime
import logging
import re

from django.core.management.base import BaseCommand
from profiles.models import Sitter, Dog, Owner, Stay

PATH = './sources/interview-jarzac-master/'
FILE = 'reviews.csv'


def parse_name(full_name):
    """Parse the name of the sitter to separate first name from last name.

    Args:
        full_name: string

    Returns:
        parsed_name: dict
    """
    full_name = full_name.split(' ')
    try:
        last_name = full_name[1]
    except IndexError:
        last_name = None

    try:
        first_name = full_name[0]
    except IndexError:
        first_name = None

    parsed_name = {'first_name': first_name,
                   'last_name': last_name,
                   }

    return parsed_name


class Command(BaseCommand):
    help = 'Imports data from csv file'

    @staticmethod
    def handle_dogs(row, owner):
        """Handles the importing of dogs and referencing to owners
         and returns a list of dogs to be referenced to a stay.

        Args:
            row: list
            owner: instance of owner

        Returns:
            dogs: list of objects
        """
        dogs = []
        for dog_name in row[5].split('|'):
            dog, saved = Dog.objects.get_or_create(
                owner_id=owner.id,
                name=dog_name,
            )
            dogs.append(dog)

        return dogs

    @staticmethod
    def handle_sitter(row):
        """Handles the import of the sitters, and returns a sitter item
        to be referenced to a stay.

        Args:
            row: list

        Returns:
            sitter: object
        """
        full_name = row[6]
        parsed_name = parse_name(full_name)
        rating = row[0]
        sitter_image = row[1]
        image = row[1]
        sitter_id = re.search('=(.*)', sitter_image).group(1)
        logging.info('sitter_id: {}'.format(sitter_id))

        sitter, saved = Sitter.objects.get_or_create(
            sitter_id=sitter_id,
            defaults={
                'first_name': parsed_name['first_name'],
                'last_name': parsed_name['last_name'],
                'rating': rating,
                'image': image,
            },
        )

        return sitter

    @staticmethod
    def handle_owner(row):
        """Handle the import of owner and returns it so it can be referenced to
        a stay.

        Args:
            row: list

        Returns:
            owner: object
        """
        full_name = row[7]
        parsed_name = parse_name(full_name)
        owner_image = row[4]
        owner_id = re.search('=(.*)', owner_image).group(1)
        logging.info('owner_id: {}'.format(owner_id))
        owner, saved = Owner.objects.get_or_create(
            owner_id=owner_id,
            defaults={
                'first_name': parsed_name['first_name'],
                'last_name': parsed_name['last_name'],
                'image': owner_image,
            },
        )

        return owner

    def handle(self, *args, **options):
        """The main handler for imports
        """
        path = PATH + FILE
        with open(path) as f:
            reader = csv.reader(f)
            next(reader)
            for row in reader:
                logging.info('Adding new row')
                sitter = self.handle_sitter(row)
                owner = self.handle_owner(row)
                dogs = self.handle_dogs(row, owner)
                start_date = datetime.datetime.strptime((row[8]), '%Y-%m-%d').strftime('%Y-%m-%d %H:%M')
                end_date = datetime.datetime.strptime((row[2]), '%Y-%m-%d').strftime('%Y-%m-%d %H:%M')
                rating = row[0]
                text = row[3]

                stay = Stay(
                            rating=rating,
                            start_date=start_date,
                            end_date=end_date,
                            sitter=sitter,
                            owner=owner,
                            text=text,
                        )

                stay.save()

                logging.info('dogs : {}'.format(dogs))
                for dog in dogs:
                    logging.info('dog : {}'.format(dog.name))
                    stay.dogs.add(dog)

