from django.contrib import admin

from .models import Sitter, Dog, Owner, Stay

admin.site.register(Sitter)
admin.site.register(Dog)
admin.site.register(Owner)
admin.site.register(Stay)
