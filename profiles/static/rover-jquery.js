$(document).ready( function() {
    $("#table").tablesorter({
    // Handles the sorting of the table. Adds sorting to columns, and adds icons.
        sortList: [[3,1]],
        headerTemplate: '{content} {icon}',
        });

    $('.dropdown-menu li a').on('click', function(e){
    // Handles the filter action.
        e.preventDefault();
        var filter = parseInt($(this).attr('tabindex'), 10);
        $('#table tr').show();
        $('#table tr').each(function (rowIndex, r) {
            var cols = [];
            $(this).find('td').each(function (colIndex, c) {
                cols.push(c.textContent);
            });
            var rank = parseInt(cols.slice(3,4), 10);
            if (rank < filter) $(this).hide();
        });
    });
});