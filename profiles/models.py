#!/usr/bin/env python
# coding: utf-8
"""
Models for the Rover exercise.

Author: Juan Arzac
Date: June, 2016
"""

from __future__ import unicode_literals

from django.db import models
from django.template.defaulttags import register


class Sitter(models.Model):
    sitter_id = models.IntegerField()
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    rating = models.IntegerField()
    image = models.CharField(max_length=200)

    def calculate_sitter_score(self):
        """Calculates the sitter score based on the number of distinct letters in the US alphabet.

        Returns:
             sitter_score: integer
        """
        full_name = self.first_name + self.last_name
        distinct_letters = set(full_name)
        distinct_count = float(len(distinct_letters))
        fraction = 1 / (26 / distinct_count)
        sitter_score = round(fraction * 5, 1)

        return sitter_score

    def calculate_rating_score(self):
        """Calculates the average rating score of a sitter.

        Returns:
            average_rating_score: integer
        """
        stays = Stay.objects.filter(sitter=self)
        count_stays = self.get_number_of_stays()
        total_ratings = 0
        for item in stays:
            total_ratings += item.rating

        if count_stays <= 0:
            return 2.60
        else:
            average_rating_score = round(total_ratings / count_stays, 1)

        return average_rating_score

    @register.filter
    def calculate_sitter_rank(self):
        """Calculates the rank of a sitter to be used to sort and filter in the List of Sitters page.

        Returns:
            sitter_rank: float
        """
        sitter_score = self.calculate_sitter_score()
        rating_score = self.calculate_rating_score()
        count_stays = self.get_number_of_stays()
        weight = max(min(10, count_stays), 0) / 10.00
        weight_sitter_score = sitter_score * (1 - weight)
        weight_rating_score = rating_score * weight
        sitter_rank = weight_sitter_score + weight_rating_score
        sitter_rank = round(sitter_rank, 1)

        return sitter_rank

    def get_number_of_stays(self):
        """ Calculates the number of stays

        Returns:
            count_stays: integer
        """
        stays = Stay.objects.filter(sitter=self)
        count_stays = len(stays)

        return count_stays


class Owner(models.Model):
    owner_id = models.IntegerField()
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    image = models.CharField(max_length=200)


class Dog(models.Model):
    name = models.CharField(max_length=200)
    owner = models.ForeignKey(Owner, on_delete=models.CASCADE)


class Stay(models.Model):
    rating = models.IntegerField()
    start_date = models.DateTimeField('start date')
    end_date = models.DateTimeField('end date')
    text = models.CharField(max_length=2000)
    sitter = models.ForeignKey(Sitter, on_delete=models.CASCADE)
    owner = models.ForeignKey(Owner, on_delete=models.CASCADE)
    dogs = models.ManyToManyField(Dog)



