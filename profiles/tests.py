#!/usr/bin/env python
# coding: utf-8
"""
Tests for Rover exercise

Author: Juan Arzac
Date: June, 2016
"""

import csv

from django.core.urlresolvers import reverse
from django.test import TestCase
from management.commands import import_data
from models import Dog
from models import Owner
from models import Sitter
from models import Stay


"""Text representing 'text' in reviews.csv"""
TEXT = '''Lorem ipsum dolor sit amet, mea eirmod vocent suscipiantur ei,
        sed no paulo ocurreret, eripuit feugait accusamus ei per.'''

"""Date for start_date and end_date"""
DATE = '2016-06-08 10:10:00+00'

PATH = './sources/interview-jarzac-master/'
FILE = 'reviews.csv'

class SitterTestCase(TestCase):
    def setUp(self):
        self.sitter = Sitter.objects.create(
            sitter_id=1,
            first_name="Dminar",
            last_name="Rygel16",
            rating="5",
            image="http://placekitten.com/g/500/500?user=305",
        )

        self.sitter2 = Sitter.objects.create(
            sitter_id=2,
            first_name="Dminar",
            last_name="Rygel16",
            rating="5",
            image="http://placekitten.com/g/500/500?user=305",
        )

        self.sitter3 = Sitter.objects.create(
            sitter_id=338,
            first_name='Lauren',
            last_name='B.',
            rating="5",
            image="http://placekitten.com/g/500/500?user=338"
        )

        self.owner = Owner.objects.create(
            owner_id=1,
            first_name="Pau",
            last_name="Zotoh",
            image="http://placekitten.com/g/500/500?user=320"
        )

        self.dog1 = Dog.objects.create(
            name='Scorpy',
            owner=self.owner
        )

        self.dog2 = Dog.objects.create(
            name="Ka D'Argo",
            owner=self.owner
        )

        self.dogs = [self.dog1, self.dog2]

        Stay.objects.create(
            rating=5,
            start_date=DATE,
            end_date=DATE,
            text=TEXT,
            sitter=self.sitter,
            owner=self.owner,
        )

        Stay.objects.create(
            rating=5,
            start_date=DATE,
            end_date=DATE,
            text=TEXT,
            sitter=self.sitter,
            owner=self.owner,
        )

        self.title = 'Rover Coding Project'
        self.full_name = 'Dominar Rygel16'
        self.first_name = 'Dominar'
        self.last_name = 'Rygel16'
        self.row = [
            '5',
            'http://placekitten.com/g/500/500?user=338',
            '',
            '',
            "http://placekitten.com/g/500/500?user=320",
            "Scorpy|Ka D'Argo",
            '',
            "Pau Zotoh"
        ]

    def test_calculate_sitter_score(self):
        """Tests that we get the correct sitter score
        """
        self.assertEqual(self.sitter.calculate_sitter_score(), 2.5)
        self.assertEqual(self.sitter2.calculate_sitter_score(), 2.5)

    def test_calculate_rating_score(self):
        """Tests that we get the correct rating score
        """
        self.assertEqual(self.sitter.calculate_rating_score(), 5)
        self.assertEqual(self.sitter2.calculate_rating_score(), 2.6)

    def test_calculate_sitter_rank(self):
        """Tests that we get the correct rating score
        """
        self.assertEqual(self.sitter.calculate_sitter_rank(), 3.00)
        self.assertEqual(self.sitter2.calculate_sitter_rank(), 2.50)

    def test_sitters_list_view(self):
        """Tests that the view is displayed
        """
        url = reverse("profiles.views.index")
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 200)
        self.assertIn(self.title, resp.content)


    def test_import_data(self):
        """Test that importing data brings the correct results
        """
        parsed_name = import_data.parse_name(self.full_name)
        first_name = parsed_name['first_name']
        last_name = parsed_name['last_name']
        self.assertEqual(first_name, self.first_name)
        self.assertEqual(last_name, self.last_name)

        returned_dogs = [self.dog1, self.dog2]
        dogs = import_data.Command.handle_dogs(self.row, self.owner)
        self.assertEqual(dogs, returned_dogs)

        path = PATH + FILE
        with open(path) as f:
            reader = csv.reader(f)
            next(reader)
            row = next(reader)

            sitter = import_data.Command.handle_sitter(row)
            self.assertEqual(sitter, self.sitter3)

        owner = import_data.Command.handle_owner(self.row)
        self.assertEqual(owner.first_name, self.owner.first_name)
        self.assertEqual(owner.last_name, self.owner.last_name)
        self.assertEqual(owner.image, self.owner.image)
        self.assertNotEqual(owner.id, self.owner.id)











