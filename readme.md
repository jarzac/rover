# Rover Coding Project - Juan Arzac

This interview exercise was performed according to these instructions:

     https://bitbucket.org/jarzac/rover/src/60b35c20dda17e20701672a7f4b62bdc4dcf75d7/sources/interview-jarzac-master/README.md?at=master&fileviewer=file-view-default
     https://github.com/roverjobs/interview-jarzac


## Data Import

The data import module extracts the stay information from the reviews.csv file and populates the tables.


## Ranking Calculations

The ranking calculations are performed in real time as the list of sitters is displayed. Another option could be to update the ranking column when new stays are created or when the rating is updated.


## Sitters List

The list of sitters uses jquery, tablesort and bootstrap for styling and functionality.
